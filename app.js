const express = require('express');
const app = express();
const mongoose = require('mongoose');
const Product = require('./models/Product')




mongoose.connect('mongodb+srv://Hugo:Golgot22@cluster0.5iaj96i.mongodb.net/?retryWrites=true&w=majority',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => console.log('Connexion à MongoDB réussie !'))
    .catch(() => console.log('Connexion à MongoDB échouée !'));


//Middleware pour résoudre le problème des CORS
//Pour accéder au corps de la requête 
app.use(express.json());


//Middleware CORS
app.use((req, res, next) => {
    //On précise que tout le monde peut accéder à l'API
    res.setHeader('Access-Control-Allow-Origin', '*');
    //On autorise l'utilisation de certains headers
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    //On autorise l'utilisation de certaines méthodes
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

//Les requêtes 
//Envoyé un produit dans la base de données
app.post('/api/products', (req, res, next) => {
    const product = new Product({
        ...req.body
    });
    product.save()
        .then(() => res.status(201).json({ message: "produit enregistré !" }))
        .catch(() => res.status(400).json({ error }))
});

//Récupérer tous les produits dans la base de donnée
app.get('/api/products', (req, res, next) => {
    Product.find()
        .then(product => res.status(200).json({ product }))
        .catch(error => res.status(400).json({ error }))
})

//Récupérer un produit avec son id 
app.get('/api/products/:id', (req, res, next) => {
    Product.findOne({ _id: req.params.id })
        .then(product => res.status(200).json({ product }))
        .catch(error => res.status(404).json({ error }))
});

//Modifier un article 
app.put('/api/products/:id', (req, res, next) => {
    Product.updateOne({ _id: req.params.id }, { ...req.body, _id: req.params.id })
        .then(() => res.status(200).json({ message: "Modified !" }))
        .catch(error => res.status(400).json({ error }))
});

//Supprimer un article
app.delete('/api/products/:id', (req, res, next) => {
    Product.deleteOne({ _id: req.params.id })
        .then(() => res.status(200).json({ message: "Deleted!" }))
        .catch(error => res.status(400).json({ error }))
});



module.exports = app;