const mongoose = require("mongoose");
//Création du schéma de données
const productSchema = mongoose.Schema({
    name: { type: String },
    description: { type: String },
    price: { type: String },
    inStock: { type: Boolean }
})

//Exportation du modèle
module.exports = mongoose.model("Product", productSchema);